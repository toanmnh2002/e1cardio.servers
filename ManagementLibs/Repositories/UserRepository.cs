﻿using BusinessObjects;
using Microsoft.EntityFrameworkCore;

namespace ManagementLibs.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly AppDbContext _context;
        public UserRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(User user)
        {
            user.CreatedDate = DateTime.UtcNow;
            user.UpdatedDate = DateTime.UtcNow;
            await _context.Users.AddAsync(user);
        }

        public void Delete(User user)
        {
            _context.Remove(user);
        }

        public async Task<User> GetByIdAsync(long id, RoleName roleName)
        {
            var user = await _context.Users
                .Include(x => x.Roles)
                .Where(x => x.Roles.Any(x => x.RoleName == roleName))
                .FirstOrDefaultAsync(x => x.Id == id);

            return user;
        }

        public async Task<User> GetByPhone(string phone)
        {
            var user = await _context.Users
                .FirstOrDefaultAsync(x => x.Phone == phone);
            return user;
        }

        public async Task<User> GetByPhoneAndPassword(string phone, string password)
        {
            var user = await _context.Users
                .Include(x => x.Roles)
                .FirstOrDefaultAsync(x =>
                    x.Phone == phone
                    && x.Password == password);
            return user;
        }

        public async Task<(RoleName, User)> GetDynamicUser(long id)
        {

            var user = await _context.Roles
                .Include(x => x.User)
                .FirstOrDefaultAsync(x => x.UserId == id);

            return (user.RoleName, user.User);
        }

        public async Task<List<User>> GetCustomersAsync(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Customer)
                .Select(x => x.User)
                .Where(x =>
                    x.Phone.Contains(query)
                    || x.FirstName.Contains(query)
                    || x.LastName.Contains(query))
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();

            return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Customer)
                .Select(x => x.User)
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<User>> GetManagersAsync(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Manager)
                .Select(x => x.User)
                .Where(x =>
                    x.Phone.Contains(query)
                    || x.FirstName.Contains(query)
                    || x.LastName.Contains(query))
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();

            return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Manager)
                .Select(x => x.User)
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<User>> GetStaffsAsync(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Staff)
                .Select(x => x.User)
                .Where(x =>
                    x.Phone.Contains(query)
                    || x.FirstName.Contains(query)
                    || x.LastName.Contains(query))
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();

            return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Staff)
                .Select(x => x.User)
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<User>> GetTrainersAsync(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Trainer)
                .Select(x => x.User)
                .Where(x =>
                    x.Phone.Contains(query)
                    || x.FirstName.Contains(query)
                    || x.LastName.Contains(query))
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();

            return await _context.Roles
                .Include(x => x.User)
                .Where(x => x.RoleName == RoleName.Trainer)
                .Select(x => x.User)
                .OrderByDescending(x => x.UpdatedDate)
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .AsNoTracking()
                .ToListAsync();
        }

        public void Update(User user)
        {
            user.UpdatedDate = DateTime.UtcNow;
            _context.Users.Update(user);
        }
    }
}
