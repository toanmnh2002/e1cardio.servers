﻿using ManagementLibs.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ManagementApis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _service;

        public TaskController(ITaskService service)
        {
            _service = service;
        }

        [Authorize(Policy = "Staff")]
        [HttpGet]
        public async Task<IActionResult> GetTasks(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetTasks(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTask(long id)
        {
            var result = await _service.GetTask(id);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTask(long id)
        {
            var result = await _service.DeleteTask(id);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTask(long id, UpdateTaskModel model)
        {
            var result = await _service.UpdateTask(id, model);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpPost]
        public async Task<IActionResult> CreateTask(CreateTaskModel model)
        {
            var result = await _service.CreateTask(model);
            return Ok(result);
        }

    }
}
