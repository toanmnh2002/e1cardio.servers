﻿using BusinessObjects;
using Microsoft.EntityFrameworkCore;

namespace ManagementLibs.Repositories
{
    public class DemoPackageRepository : IDemoPackageRepository
    {
        private readonly AppDbContext _context;
        public DemoPackageRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(DemoPackage demoPackage)
        {
            demoPackage.CreatedDate = DateTime.UtcNow;
            demoPackage.UpdatedDate = DateTime.UtcNow;
            await _context.DemoPackages.AddAsync(demoPackage);
        }

        public void Delete(DemoPackage demoPackage)
        {
            _context.DemoPackages.Remove(demoPackage);
        }

        public async Task<DemoPackage> GetDemoPackageAsync(long id)
        {
            return await _context.DemoPackages
                .Include(x => x.Branch)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<DemoPackage>> GetDemoPackagesAsync(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.DemoPackages
                    .Include(x => x.Branch)
                    .OrderByDescending(x => x.UpdatedDate)
                    .Where(x =>
                        x.PackageName.Contains(query)
                        || x.PackagePrice.ToString().Contains(query)
                        || x.Type.Contains(query)
                        || (x.Branch == null 
                        || x.Branch.BranchName.Contains(query) 
                        || x.Branch.Location.Contains(query)))
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync();

            return await _context.DemoPackages
                    .Include(x => x.Branch)
                    .OrderByDescending(x => x.UpdatedDate)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync();
        }

        public void Update(DemoPackage demoPackage)
        {
            _context.DemoPackages.Update(demoPackage);
        }
    }
}
