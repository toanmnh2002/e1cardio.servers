﻿using AutoMapper;
using BusinessObjects;

namespace ManagementLibs.Services
{
    public class BranchService : IBranchService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public BranchService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
        public async Task<ResponseModel<BranchModel>> CreateBranch(CreateBranchModel model)
        {
            // validate model
            var branch = _mapper.Map<Branch>(model);
            await _unitOfWork.BranchRepository.CreateAsync(branch);
            if (await _unitOfWork.SaveChangeAsync() > 0)
                return new ResponseModel<BranchModel>
                {
                    Data = _mapper.Map<BranchModel>(branch)
                };

            return new ResponseModel<BranchModel>
            {
                Errors = "Create fail"
            };
        }

        public async Task<ResponseModel<bool>> DeleteBranch(long id)
        {
            var branch = await _unitOfWork.BranchRepository.GetAsync(id);
            if (branch == null)
                return new ResponseModel<bool>
                {
                    Data = true
                };
            _unitOfWork.BranchRepository.Delete(branch);
            if (await _unitOfWork.SaveChangeAsync() > 0)
                return new ResponseModel<bool>
                {
                    Data = true
                };

            return new ResponseModel<bool>
            {
                Errors = "Delete fail"
            };
        }

        public async Task<ResponseModel<BranchModel>> GetBranchById(long branchId)
        {
            var branch = await _unitOfWork.BranchRepository.GetAsync(branchId);
            if (branch == null)
                return new ResponseModel<BranchModel>
                {
                    Errors = "Not found"
                };

            return new ResponseModel<BranchModel>
            {
                Data = _mapper.Map<BranchModel>(branch)
            };
        }

        public async Task<ResponseModel<List<BranchModel>>> GetBranches(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var branches = await _unitOfWork.BranchRepository.GetBranches(query, pageIndex, pageSize);

            return new ResponseModel<List<BranchModel>>
            {
                Data = _mapper.Map<List<BranchModel>>(branches)
            };
        }

        public async Task<ResponseModel<BranchModel>> UpdateBranch(long id, CreateBranchModel model)
        {
            // validate model
            var branch = await _unitOfWork.BranchRepository.GetAsync(id);
            if (branch == null)
                return new ResponseModel<BranchModel>
                {
                    Errors = "Not found"
                };

            branch.BranchName = model.BranchName;
            branch.Phone = model.Phone;
            branch.Location = model.Location;

            _unitOfWork.BranchRepository.Update(branch);
            if (await _unitOfWork.SaveChangeAsync() > 0)
                return new ResponseModel<BranchModel>
                {
                    Data = _mapper.Map<BranchModel>(branch)
                };

            return new ResponseModel<BranchModel>
            {
                Errors = "Update fail"
            };
        }
    }
}
