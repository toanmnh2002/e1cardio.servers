﻿using BusinessObjects;

namespace ManagementLibs.Repositories
{
    public interface IUserRepository
    {
        Task CreateAsync(User user);
        void Update(User user);
        void Delete(User user);
        Task<(RoleName, User)> GetDynamicUser(long id);
        Task<User> GetByIdAsync(long id, RoleName roleName);
        Task<User> GetByPhone(string phone);
        Task<User> GetByPhoneAndPassword(string phone, string password);
        Task<List<User>> GetCustomersAsync(string? query, int pageIndex = 1, int pageSize = 10);
        Task<List<User>> GetStaffsAsync(string? query, int pageIndex = 1, int pageSize = 10);
        Task<List<User>> GetTrainersAsync(string? query, int pageIndex = 1, int pageSize = 10);
        Task<List<User>> GetManagersAsync(string? query, int pageIndex = 1, int pageSize = 10);
    }
}
