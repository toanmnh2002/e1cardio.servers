﻿using BusinessObjects;

public class DemoPackageModel
{
    public long Id { get; set; }
    public string? ImageUrl { get; set; }
    public string PackageName { get; set; }
    public string Descriptions { get; set; }
    public double NumberOfDays { get; set; }
    public double NumberOfSessions { get; set; }
    public decimal PackagePrice { get; set; }
    public string Type { get; set; }
    public BranchModel? Branch { get; set; }
}

public class CreateDemoPackageModel
{
    public string? ImageUrl { get; set; }
    public string PackageName { get; set; }
    public string Descriptions { get; set; }
    public double NumberOfDays { get; set; }
    public double NumberOfSessions { get; set; }
    public decimal PackagePrice { get; set; }
    public string Type { get; set; }
    public long BranchId { get; set; }
}

public class UpdateDemoPackageModel
{
    public string? ImageUrl { get; set; }
    public string PackageName { get; set; }
    public string Descriptions { get; set; }
    public double NumberOfDays { get; set; }
    public double NumberOfSessions { get; set; }
    public decimal PackagePrice { get; set; }
    public string Type { get; set; }
    public long BranchId { get; set; }
}