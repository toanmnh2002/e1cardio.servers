﻿namespace ManagementLibs.Services
{
    public interface IBranchService
    {
        Task<ResponseModel<BranchModel>> GetBranchById(long branchId);
        Task<ResponseModel<List<BranchModel>>> GetBranches(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<BranchModel>> CreateBranch(CreateBranchModel branch);
        Task<ResponseModel<BranchModel>> UpdateBranch(long id, CreateBranchModel branch);
        Task<ResponseModel<bool>> DeleteBranch(long id);
    }
}
