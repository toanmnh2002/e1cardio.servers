﻿public class JWTSection
{
    public string SecretKey { get; set; }
    public int ExpiresInDays { get; set; }
}