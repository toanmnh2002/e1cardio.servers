﻿using BusinessObjects;

public class PackageConsultantModel
{
    public long Id { get; set; }
    public DemoPackageModel DemoPackage { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public List<Schedule> Schedules { get; set; }
    public bool IsReceived { get; set; }
    public ConsultantModel Consultants { get; set; }
    public ConsultantModel Consultees { get; set; }
}

public class CreatePackageConsultantModel
{
    public long DemoPackageId { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public List<Schedule> Schedules { get; set; }
    public CustomerInfomation Consultee { get; set; }
}

public class UpdatePackageConsultantModel
{
    public long DemoPackageId { get; set; }
    public DateTime StartDate { get; set; }
    public DateTime EndDate { get; set; }
    public List<Schedule> Schedules { get; set; }
    public bool IsReceived { get; set; }
}

public class CustomerInfomation
{
    public string Phone { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
}

public class ConsultantModel
{
    public string Phone { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
}

public class ConsulteeModel
{
    public string Phone { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
}