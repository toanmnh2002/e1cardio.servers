﻿namespace BusinessObjects
{
    public class TrainingTracking : BaseEntity
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long SessionId { get; set; }
        public Session? Session { get; set; }
        public DateTime FinishedDate { get; set; }
        public long EnergyPoint { get; set; }
    }
}
