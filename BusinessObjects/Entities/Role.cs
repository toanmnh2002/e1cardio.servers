﻿namespace BusinessObjects
{
    public class Role : BaseEntity
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public RoleName RoleName { get; set; }
    }
}
