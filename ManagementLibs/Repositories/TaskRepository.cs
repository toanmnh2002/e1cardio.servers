﻿using BusinessObjects;
using Microsoft.EntityFrameworkCore;
using TaskStatus = BusinessObjects.TaskStatus;

namespace ManagementLibs.Repositories
{
    public class TaskRepository : ITaskRepository
    {
        private readonly AppDbContext _context;

        public TaskRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<List<Tasks>> GetTasks(string? query, long currentUserId, int pageIndex = 1, int pageSize = 10, bool isManager = false)
        {

            var tasks = new List<Tasks>();
            var taskStatus = Enum.GetValues(typeof(TaskStatus))
               .Cast<TaskStatus>()
               .Select(x => x.ToString())
               .ToList();

            var taskPriority = Enum.GetValues(typeof(TaskPriority))
               .Cast<TaskPriority>()
               .Select(x => x.ToString())
               .ToList();

            if (isManager)
            {
                if (!string.IsNullOrEmpty(query))
                    tasks = await _context.Tasks
                    .Include(x => x.Assigners)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Receivers)
                    .ThenInclude(x => x.User)
                    .Where(x =>
                    x.TaskName.Contains(query)
                    || (taskStatus.Contains(query) && x.Status == (TaskStatus)Enum.Parse(typeof(TaskStatus), query))
                    || (taskPriority.Contains(query) && x.Priority == (TaskPriority)Enum.Parse(typeof(TaskPriority), query))
                    || x.Receivers.Any(x => x.User.FirstName.Contains(query)
                    || x.User.FirstName.Contains(query)
                    || x.User.LastName.Contains(query)
                    || x.User.Phone.Contains(query)))
                    .OrderByDescending(x => x.Priority)
                    .ThenBy(x => x.Status)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .AsSplitQuery()
                    .ToListAsync();
                else
                {
                    tasks = await _context.Tasks
                    .Include(x => x.Assigners)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Receivers)
                    .ThenInclude(x => x.User)
                    .OrderByDescending(x => x.Priority)
                    .ThenBy(x => x.Status)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .AsSplitQuery()
                    .ToListAsync();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(query))
                {
                    tasks = await _context.Tasks
                    .Include(x => x.Assigners)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Receivers)
                    .ThenInclude(x => x.User)
                    .Where(x => x.Assigners.Any(assigner => assigner.UserId == currentUserId))
                    .Where(x =>
                    x.TaskName.Contains(query)
                    || (taskStatus.Contains(query) && x.Status == (TaskStatus)Enum.Parse(typeof(TaskStatus), query))
                    || (taskPriority.Contains(query) && x.Priority == (TaskPriority)Enum.Parse(typeof(TaskPriority), query))
                    || x.Receivers.Any(x => x.User.FirstName.Contains(query)
                    || x.User.FirstName.Contains(query)
                    || x.User.LastName.Contains(query)
                    || x.User.Phone.Contains(query)))
                    .OrderByDescending(x => x.Priority)
                    .ThenBy(x => x.Status)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .AsSplitQuery()
                    .ToListAsync();
                }
                else
                {
                    tasks = await _context.Tasks
                    .Include(x => x.Assigners)
                    .ThenInclude(x => x.User)
                    .Include(x => x.Receivers)
                    .ThenInclude(x => x.User)
                    .Where(x => x.Assigners.Any(assigner => assigner.UserId == currentUserId))
                    .OrderByDescending(x => x.Priority)
                    .ThenBy(x => x.Status)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .AsSplitQuery()
                    .ToListAsync();
                }
            }

            return tasks;
        }

        public async Task<Tasks> GetAsync(long id)
        {
            return await _context.Tasks
                .Include(x => x.Assigners)
                .ThenInclude(x => x.User)
                .Include(x => x.Receivers)
                .ThenInclude(x => x.User)
                .AsSplitQuery()
                .FirstOrDefaultAsync(x => x.Id == id);
        }
        public async Task CreateAsync(Tasks task)
        {
            task.CreatedDate = DateTime.UtcNow;
            task.UpdatedDate = DateTime.UtcNow;
            await _context.Tasks.AddAsync(task);
        }

        public void Delete(Tasks task) => _context.Tasks.Remove(task);

        public void Update(Tasks task)
        {
            task.UpdatedDate = DateTime.UtcNow;
            _context.Tasks.Update(task);
        }
    }
}
