﻿using Microsoft.EntityFrameworkCore;

namespace BusinessObjects
{
    public class AppDbContext(DbContextOptions<AppDbContext> options) : DbContext(options)
    {
        #region DB Set
        public DbSet<Branch> Branches { get; set; }
        public DbSet<DemoPackage> DemoPackages { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<SessionItem> SessionItems { get; set; }
        public DbSet<PackageConsultant> PackageConsultants { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<ScheduleChangeRequest> ScheduleChangeRequests { get; set; }
        public DbSet<Tasks> Tasks { get; set; }
        public DbSet<TrainingTracking> TrainingTrackings { get; set; }
        public DbSet<User> Users { get; set; }
        // relationship
        public DbSet<PackageCreator> PackageCreators { get; set; }
        public DbSet<PackageTrainer> PackageTrainers { get; set; }
        public DbSet<PackageFollower> PackageFollowers { get; set; }
        public DbSet<SessionTrainer> SessionTrainers { get; set; }
        public DbSet<Consultants> Consultants { get; set; }
        public DbSet<Consultees> Consultees { get; set; }
        public DbSet<TaskAssigner> TaskAssigners { get; set; }
        public DbSet<TaskReceiver> TaskReceivers { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Branch>()
                .HasMany(x => x.DemoPackages)
                .WithOne(x => x.Branch)
                .HasForeignKey(x => x.BranchId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Branch>()
                .HasMany(x => x.Packages)
                .WithOne(x => x.Branch)
                .HasForeignKey(x => x.BranchId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Package>()
                .HasMany(x => x.Sessions)
                .WithOne(x => x.Package)
                .HasForeignKey(x => x.PackageId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Package>()
                .HasMany(x => x.PackageCreators)
                .WithOne(x => x.Package)
                .HasForeignKey(x => x.PackageId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Package>()
                .HasMany(x => x.PackageTrainers)
                .WithOne(x => x.Package)
                .HasForeignKey(x => x.PackageId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Package>()
                .HasMany(x => x.PackageFollowers)
                .WithOne(x => x.Package)
                .HasForeignKey(x => x.PackageId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Session>()
                .HasMany(x => x.SessionItems)
                .WithOne(x => x.Session)
                .HasForeignKey(x => x.SessionId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Session>()
                .HasMany(x => x.SessionTrainers)
                .WithOne(x => x.Session)
                .HasForeignKey(x => x.SessionId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<DemoPackage>()
                .HasMany(x => x.PackageConsultants)
                .WithOne(x => x.DemoPackage)
                .HasForeignKey(x => x.DemoPackageId)
                .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<PackageConsultant>()
                .HasMany(x => x.Consultants)
                .WithOne(x => x.PackageConsultant)
                .HasForeignKey(x => x.PackageConsultantId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<PackageConsultant>()
                .HasMany(x => x.Consultees)
                .WithOne(x => x.PackageConsultant)
                .HasForeignKey(x => x.PackageConsultantId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Tasks>()
                .HasMany(x => x.Assigners)
                .WithOne(x => x.Task)
                .HasForeignKey(x => x.TaskId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Tasks>()
                .HasMany(x => x.Receivers)
                .WithOne(x => x.Task)
                .HasForeignKey(x => x.TaskId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Roles)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Consultants)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.Consultees)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.PackageCreators)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.PackageTrainers)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.PackageFollowers)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.SessionTrainers)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
                .HasMany(x => x.ScheduleChanges)
                .WithOne(x => x.User)
                .HasForeignKey(x => x.UserId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
               .HasMany(x => x.TaskAssigners)
               .WithOne(x => x.User)
               .HasForeignKey(x => x.UserId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
               .HasMany(x => x.TaskReceivers)
               .WithOne(x => x.User)
               .HasForeignKey(x => x.UserId)
               .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
              .HasMany(x => x.TrainingTrackings)
              .WithOne(x => x.User)
              .HasForeignKey(x => x.UserId)
              .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<User>()
              .HasIndex(x => x.Phone)
              .IsUnique();

            modelBuilder.Entity<Order>()
                .HasOne(x => x.User)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.UserId);

            // m-m config
            modelBuilder.Entity<PackageCreator>()
                .HasKey(x => new { x.PackageId, x.UserId });

            modelBuilder.Entity<PackageTrainer>()
                .HasKey(x => new { x.PackageId, x.UserId });

            modelBuilder.Entity<PackageFollower>()
                .HasKey(x => new { x.PackageId, x.UserId });

            modelBuilder.Entity<SessionTrainer>()
                .HasKey(x => new { x.SessionId, x.UserId });

            modelBuilder.Entity<Consultants>()
                .HasKey(x => new { x.PackageConsultantId, x.UserId });

            modelBuilder.Entity<Consultees>()
                .HasKey(x => new { x.PackageConsultantId, x.UserId });

            modelBuilder.Entity<TaskAssigner>()
                .HasKey(x => new { x.TaskId, x.UserId });

            modelBuilder.Entity<TaskReceiver>()
                .HasKey(x => new { x.TaskId, x.UserId });
        }
    }
}
