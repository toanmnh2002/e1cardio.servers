﻿namespace ManagementLibs.Services
{
    public interface IDemoPackageService
    {
        Task<ResponseModel<DemoPackageModel>> GetPackage(long id);
        Task<ResponseModel<List<DemoPackageModel>>> GetPackages(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<DemoPackageModel>> CreateDemoPackage(CreateDemoPackageModel model);
        Task<ResponseModel<DemoPackageModel>> UpdateDemoPackage(long id, UpdateDemoPackageModel model);
        Task<ResponseModel<bool>> DeleteDemoPackage(long id);
    }
}
