﻿using ManagementLibs.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ManagementApis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoPackageController : ControllerBase
    {
        private readonly IDemoPackageService _service;
        public DemoPackageController(IDemoPackageService service)
        {
            _service = service;
        }

        [Authorize(Policy = "Staff")]
        [HttpGet]
        public async Task<IActionResult> GetDemoPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetPackages(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDemoPackage(long id)
        {
            var result = await _service.GetPackage(id);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPost]
        public async Task<IActionResult> CreateDemoPackage([FromBody] CreateDemoPackageModel model)
        {
            var result = await _service.CreateDemoPackage(model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDemoPackage(long id, [FromBody] UpdateDemoPackageModel model)
        {
            var result = await _service.UpdateDemoPackage(id, model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDemoPackage(long id)
        {
            var result = await _service.DeleteDemoPackage(id);
            return Ok(result);
        }
    }
}
