﻿namespace ManagementLibs.Services
{
    public interface IPackageConsultantService
    {
        Task<ResponseModel<PackageConsultantModel>> CreatePackageConsultant(CreatePackageConsultantModel model);
        Task<ResponseModel<PackageConsultantModel>> UpdateConsultant(long id, UpdatePackageConsultantModel model);
        Task<ResponseModel<List<PackageConsultantModel>>> GetPackageConsultants(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<PackageConsultantModel>> GetPackageConsultant(long id);
        Task<ResponseModel<bool>> DeleteConsultant(long id);
    }
}
