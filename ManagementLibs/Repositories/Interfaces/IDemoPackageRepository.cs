﻿using BusinessObjects;

namespace ManagementLibs.Repositories
{
    public interface IDemoPackageRepository
    {
        Task CreateAsync(DemoPackage demoPackage);
        void Update(DemoPackage demoPackage);
        void Delete(DemoPackage demoPackage);
        Task<List<DemoPackage>> GetDemoPackagesAsync(string? query, int pageIndex = 1, int pageSize = 10);
        Task<DemoPackage> GetDemoPackageAsync(long id);
    }
}
