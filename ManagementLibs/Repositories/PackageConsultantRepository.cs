﻿using BusinessObjects;
using Microsoft.EntityFrameworkCore;

namespace ManagementLibs.Repositories
{
    public class PackageConsultantRepository : IPackageConsultantRepository
    {
        private readonly AppDbContext _context;
        public PackageConsultantRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(PackageConsultant obj)
        {
            obj.CreatedDate = DateTime.UtcNow;
            obj.UpdatedDate = DateTime.UtcNow;
            obj.IsReceived = false;
            await _context.AddAsync(obj);
        }

        public void Delete(PackageConsultant obj)
        {
            _context.PackageConsultants.Remove(obj);
        }

        public async Task<PackageConsultant> GetAsync(long id)
        {
            return await _context.PackageConsultants
                .Include(x => x.DemoPackage)
                .ThenInclude(x => x.Branch)
                .Include(x => x.Consultants)
                .ThenInclude(x => x.User)
                .Include(x => x.Consultees)
                .ThenInclude(x => x.User)
                .AsSplitQuery()
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<PackageConsultant>> GetPackageConsultants(string? query,
            long userId,
            int pageIndex = 1,
            int pageSize = 10,
            bool isManager = false)
        {
            var consultants = new List<PackageConsultant>();
            if (isManager)
            {
                if (!string.IsNullOrEmpty(query))
                {
                    consultants = await _context.PackageConsultants
                        .Include(x => x.DemoPackage)
                        .ThenInclude(x => x.Branch)
                        .Include(x => x.Consultants)
                        .ThenInclude(x => x.User)
                        .Include(x => x.Consultees)
                        .ThenInclude(x => x.User)
                        .Where(x =>
                            x.DemoPackage.PackageName.Contains(query)
                            || x.Consultees.Any(x => x.User.FirstName.Contains(query)
                            || x.User.LastName.Contains(query)
                            || x.User.Phone.Contains(query)))
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .AsSplitQuery()
                        .ToListAsync();
                }
                else
                {
                    consultants = await _context.PackageConsultants
                        .Include(x => x.DemoPackage)
                        .ThenInclude(x => x.Branch)
                        .Include(x => x.Consultants)
                        .ThenInclude(x => x.User)
                        .Include(x => x.Consultees)
                        .ThenInclude(x => x.User)
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .AsSplitQuery()
                        .ToListAsync();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(query))
                {
                    consultants = await _context.PackageConsultants
                        .Include(x => x.DemoPackage)
                        .ThenInclude(x => x.Branch)
                        .Include(x => x.Consultants)
                        .ThenInclude(x => x.User)
                        .Include(x => x.Consultees)
                        .ThenInclude(x => x.User)
                        .Where(x => x.Consultants.Any(user => user.UserId == userId))
                        .Where(x =>
                            x.DemoPackage.PackageName.Contains(query)
                            || x.Consultees.Any(x => x.User.FirstName.Contains(query)
                            || x.User.LastName.Contains(query)
                            || x.User.Phone.Contains(query)))
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .AsSplitQuery()
                        .ToListAsync();
                }
                else
                {
                    consultants = await _context.PackageConsultants
                        .Include(x => x.DemoPackage)
                        .ThenInclude(x => x.Branch)
                        .Include(x => x.Consultants)
                        .ThenInclude(x => x.User)
                        .Include(x => x.Consultees)
                        .ThenInclude(x => x.User)
                        .Where(x => x.Consultants.Any(user => user.UserId == userId))
                        .Skip((pageIndex - 1) * pageSize)
                        .Take(pageSize)
                        .AsNoTracking()
                        .AsSplitQuery()
                        .ToListAsync();
                }
            }

            return consultants;
        }

        public void Update(PackageConsultant obj)
        {
            obj.UpdatedDate = DateTime.UtcNow;
            _context.PackageConsultants.Update(obj);
        }
    }
}
