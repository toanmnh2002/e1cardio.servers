﻿public class BranchModel
{
    public long Id { get; set; }
    public string BranchName { get; set; }
    public string Location { get; set; }
    public string Phone { get; set; }
}
public class CreateBranchModel
{
    public string BranchName { get; set; }
    public string Location { get; set; }
    public string Phone { get; set; }
}