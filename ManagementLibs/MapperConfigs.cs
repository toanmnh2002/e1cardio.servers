﻿using AutoMapper;
using BusinessObjects;

namespace ManagementLibs
{
    public class MapperConfigs : Profile
    {
        public MapperConfigs()
        {
            CreateMap<UserModel, User>().ReverseMap();

            CreateMap<CustomerModel, User>().ReverseMap();
            CreateMap<CreateCustomerModel, User>().ReverseMap();

            CreateMap<ManagerModel, User>().ReverseMap();
            CreateMap<CreateManagerModel, User>().ReverseMap();

            CreateMap<CreateBranchModel, Branch>().ReverseMap();
            CreateMap<BranchModel, Branch>().ReverseMap();

            CreateMap<StaffModel, User>().ReverseMap();
            CreateMap<CreateStaffModel, User>().ReverseMap();

            CreateMap<TrainerModel, User>().ReverseMap();
            CreateMap<CreateTrainerModel, User>().ReverseMap();

            CreateMap<ConsultantModel, User>().ReverseMap();
            CreateMap<ConsulteeModel, User>().ReverseMap();

            CreateMap<DemoPackageModel, DemoPackage>().ReverseMap();
            CreateMap<CreateDemoPackageModel, DemoPackage>().ReverseMap();

            CreateMap<TaskAssignerModel, User>().ReverseMap();
            CreateMap<TaskReceiverModel, User>().ReverseMap();
            //CreateMap<UpdateCustomerModel, Customer>().ReverseMap();
            //CreateMap<TrainerModel, BusinessObjects.Trainer>().ReverseMap();

            //CreateMap<DemoPackage, Package>().ReverseMap();
            //CreateMap<PackageModel, Package>()
            //        .ForMember(des => des.DefaultSchedule,
            //                   act => act.MapFrom(src => JsonConvert.SerializeObject(src.Schedules)))
            //        .ReverseMap()
            //        .ForMember(des => des.Schedules,
            //                   act => act.MapFrom(src => JsonConvert.DeserializeObject<List<ScheduleModel>>(src.DefaultSchedule)));
            //CreateMap<PackageDetailsModel, Package>()
            //    .ForMember(des => des.DefaultSchedule,
            //               act => act.MapFrom(src => JsonConvert.SerializeObject(src.Schedules)))
            //    .ReverseMap()
            //    .ForMember(des => des.Schedules,
            //               act => act.MapFrom(src => JsonConvert.DeserializeObject<List<ScheduleModel>>(src.DefaultSchedule))); ;
            //CreateMap<SessionModel, Session>().ReverseMap();
            //CreateMap<SessionItemModel, SessionItem>().ReverseMap();

            //CreateMap<UpdatePackageModel, Package>()
            //        .ForMember(des => des.DefaultSchedule,
            //                   act => act.MapFrom(src => JsonConvert.SerializeObject(src.Schedules)))
            //        .ReverseMap()
            //        .ForMember(des => des.Schedules,
            //                   act => act.MapFrom(src => JsonConvert.DeserializeObject<List<ScheduleModel>>(src.DefaultSchedule))); ;
            //CreateMap<UpdateSessionModel, Session>();
            //CreateMap<UpdateSessionItemModel, SessionItem>();
            //CreateMap<DemoPackageModel, DemoPackage>().ReverseMap();
            //CreateMap<BranchModel, Branch>().ReverseMap();
        }
    }
}
