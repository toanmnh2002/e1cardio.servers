﻿using BusinessObjects;
using ManagementLibs.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ManagementLibs
{
    public static class Extentions
    {
        public static IServiceCollection AddLibsServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(option => option.UseNpgsql(configuration.GetConnectionString("E1Cardio")));
            services.AddSingleton(configuration.GetSection("JWTSection").Get<JWTSection>());
            services.AddAutoMapper(typeof(MapperConfigs).Assembly);

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IBranchService, BranchService>();
            services.AddScoped<IDemoPackageService, DemoPackageService>();
            services.AddScoped<IPackageConsultantService, PackageConsultantService>();
            services.AddScoped<ITaskService, TaskService>();

            return services;
        }
    }
}
