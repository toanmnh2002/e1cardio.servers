﻿using ManagementLibs.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ManagementApis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageConsultantController : ControllerBase
    {
        private readonly IPackageConsultantService _service;
        public PackageConsultantController(IPackageConsultantService service)
        {
            _service = service;
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetConsultant(long id)
        {
            var result = await _service.GetPackageConsultant(id);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpGet]
        public async Task<IActionResult> GetConsultants(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetPackageConsultants(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpPost]
        public async Task<IActionResult> CreateNewConsultant([FromBody] CreatePackageConsultantModel model)
        {
            var result = await _service.CreatePackageConsultant(model);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateConsultant(long id, [FromBody] UpdatePackageConsultantModel model)
        {
            var result = await _service.UpdateConsultant(id, model);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteConsultant(long id)
        {
            var result = await _service.DeleteConsultant(id);
            return Ok(result);
        }
    }
}
