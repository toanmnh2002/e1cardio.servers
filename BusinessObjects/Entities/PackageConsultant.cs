﻿namespace BusinessObjects
{
    public class PackageConsultant : BaseEntity
    {
        public long DemoPackageId { get; set; }
        public DemoPackage DemoPackage { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Schedules { get; set; }
        public bool IsReceived { get; set; }
        public ICollection<Consultants> Consultants { get; set; }
        public ICollection<Consultees> Consultees { get; set; }
    }
    public class Consultants
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long PackageConsultantId { get; set; }
        public PackageConsultant PackageConsultant { get; set; }
    }
    public class Consultees
    {
        public long UserId { get; set; }
        public User User { get; set; }
        public long PackageConsultantId { get; set; }
        public PackageConsultant PackageConsultant { get; set; }
    }
}
