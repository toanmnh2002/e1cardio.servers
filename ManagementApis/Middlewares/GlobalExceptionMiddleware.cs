﻿using System.Text.Json;

public class GlobalExceptionMiddleware
{
    private readonly RequestDelegate _next;

    public GlobalExceptionMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (Exception ex)
        {
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            var response = new
            {
                StatusCode = 500,
                ErrorMessage = ex.Message,
            };
            string json = JsonSerializer.Serialize(response);
            await context.Response.WriteAsync(json);
        }
    }
}