﻿using BusinessObjects;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace ManagementLibs
{
    public static class JwtHelper
    {
        public static string GenerateToken(this User user, JWTSection jwt)
        {
            var roles = user.GetRoles();
            if (roles.Count > 0)
            {
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwt.SecretKey));
                var creds = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
                // claims
                var claims = new List<Claim>();
                foreach (var role in roles)
                {
                    claims.Add(new Claim("roles", role));
                }
                claims.Add(new Claim("id", user.Id.ToString()));
                claims.Add(new Claim("phone", user.Phone));

                var token = new JwtSecurityToken(
                        claims: claims,
                        expires: DateTime.Now.AddDays(Convert.ToDouble(jwt.ExpiresInDays)),
                        signingCredentials: creds);

                return new JwtSecurityTokenHandler().WriteToken(token);
            }

            return null;
        }
        private static List<string> GetRoles(this User user)
        {
            return user.Roles
                .Select(x => x.RoleName.ToString())
                .ToList() ?? [];
        }
    }
}
