﻿using AutoMapper;
using BusinessObjects;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ManagementLibs.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IHttpContextAccessor _httpContext;
        private readonly JWTSection _jwtSection;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork,
            IHttpContextAccessor httpContext,
            JWTSection jwtSection,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _httpContext = httpContext;
            _jwtSection = jwtSection;
            _mapper = mapper;
        }

        public async Task<ResponseModel<dynamic>> GetCurrentUser()
        {
            var currentUserId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentUserId == null)
                return new ResponseModel<dynamic>
                {
                    Errors = "Not login yet"
                };
            _ = long.TryParse(currentUserId, out long id);
            var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);
            switch (role)
            {
                case RoleName.Manager:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<ManagerModel>(user)
                    };
                case RoleName.Staff:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<StaffModel>(user)
                    };
                case RoleName.Trainer:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<TrainerModel>(user)
                    };
                case RoleName.Customer:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<CustomerModel>(user)
                    };
                default:
                    break;
            }

            return new ResponseModel<dynamic>
            {
                Errors = "Not login yet"
            };
        }

        public async Task<ResponseModel<List<CustomerModel>>> GetCustomers(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var resData = new List<CustomerModel>();
            var customers = await _unitOfWork.UserRepository.GetCustomersAsync(query, pageIndex, pageSize);
            resData = _mapper.Map<List<CustomerModel>>(customers);
            resData.ForEach(customer =>
            {
                customer.Password = null;
            });

            return new ResponseModel<List<CustomerModel>>
            {
                Data = resData
            };
        }

        public async Task<ResponseModel<List<ManagerModel>>> GetManagers(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var resData = new List<ManagerModel>();
            var managers = await _unitOfWork.UserRepository.GetManagersAsync(query, pageIndex, pageSize);
            resData = _mapper.Map<List<ManagerModel>>(managers);
            resData.ForEach(manager =>
            {
                manager.Password = null;
            });

            return new ResponseModel<List<ManagerModel>>
            {
                Data = resData
            };
        }

        public async Task<ResponseModel<List<StaffModel>>> GetStaffs(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var resData = new List<StaffModel>();
            var staffs = await _unitOfWork.UserRepository.GetStaffsAsync(query, pageIndex, pageSize);
            resData = _mapper.Map<List<StaffModel>>(staffs);
            resData.ForEach(staff =>
            {
                staff.Password = null;
            });

            return new ResponseModel<List<StaffModel>>
            {
                Data = resData
            };
        }

        public async Task<ResponseModel<List<TrainerModel>>> GetTrainers(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var resData = new List<TrainerModel>();
            var trainers = await _unitOfWork.UserRepository.GetTrainersAsync(query, pageIndex, pageSize);
            resData = _mapper.Map<List<TrainerModel>>(trainers);
            resData.ForEach(trainer =>
            {
                trainer.Password = null;
            });

            return new ResponseModel<List<TrainerModel>>
            {
                Data = resData
            };
        }

        public async Task<ResponseModel<dynamic>> GetUser(long id)
        {
            var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);
            switch (role)
            {
                case RoleName.Manager:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<ManagerModel>(user)
                    };
                case RoleName.Staff:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<StaffModel>(user)
                    };
                case RoleName.Trainer:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<TrainerModel>(user)
                    };
                case RoleName.Customer:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<CustomerModel>(user)
                    };
                default:
                    break;
            }

            return new ResponseModel<dynamic>
            {
                Errors = "Not login yet"
            };
        }

        public async Task<ResponseModel<string>> Login(LoginModel model)
        {
            // validate model
            var user = await _unitOfWork.UserRepository.GetByPhoneAndPassword(model.Phone, model.Password);
            if (user == null)
            {
                return new ResponseModel<string>
                {
                    Errors = "Not found user"
                };
            }

            return new ResponseModel<string>
            {
                Data = user.GenerateToken(_jwtSection)
            };
        }

        public async Task<ResponseModel<CustomerModel>> NewCustomer(CreateCustomerModel model)
        {
            // validate model
            var user = _mapper.Map<User>(model);

            user.Roles ??= [];
            user.Roles.Add(new Role
            {
                RoleName = RoleName.Customer,
                CreatedDate = DateTime.UtcNow,
                UpdatedDate = DateTime.UtcNow,
            });

            await _unitOfWork.UserRepository.CreateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                var resData = _mapper.Map<CustomerModel>(user);
                resData.Password = null;

                return new ResponseModel<CustomerModel>
                {
                    Data = resData
                };
            }

            return new ResponseModel<CustomerModel>
            {
                Errors = "Fail"
            };
        }

        public async Task<ResponseModel<ManagerModel>> NewManager(CreateManagerModel model)
        {
            // validate model
            var user = _mapper.Map<User>(model);

            user.Roles ??= [];
            var roles = new List<Role>
            {
                new() {
                    RoleName = RoleName.Manager,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                }
            };
            roles.ForEach(role => user.Roles.Add(role));

            await _unitOfWork.UserRepository.CreateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                var resData = _mapper.Map<ManagerModel>(user);
                resData.Password = null;

                return new ResponseModel<ManagerModel>
                {
                    Data = resData
                };
            }

            return new ResponseModel<ManagerModel>
            {
                Errors = "Fail"
            };
        }

        public async Task<ResponseModel<StaffModel>> NewStaff(CreateStaffModel model)
        {
            // validate model
            var user = _mapper.Map<User>(model);

            user.Roles ??= [];
            var roles = new List<Role>
            {
                new() {
                    RoleName = RoleName.Staff,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                }
            };
            roles.ForEach(role => user.Roles.Add(role));

            await _unitOfWork.UserRepository.CreateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                var resData = _mapper.Map<StaffModel>(user);
                resData.Password = null;

                return new ResponseModel<StaffModel>
                {
                    Data = resData
                };
            }

            return new ResponseModel<StaffModel>
            {
                Errors = "Fail"
            };
        }

        public async Task<ResponseModel<TrainerModel>> NewTrainer(CreateTrainerModel model)
        {
            // validate model
            var user = _mapper.Map<User>(model);

            user.Roles ??= [];
            var roles = new List<Role>
            {
                new() {
                    RoleName = RoleName.Trainer,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                }
            };
            roles.ForEach(role => user.Roles.Add(role));

            await _unitOfWork.UserRepository.CreateAsync(user);
            var isSuccess = await _unitOfWork.SaveChangeAsync();
            if (isSuccess > 0)
            {
                var resData = _mapper.Map<TrainerModel>(user);
                resData.Password = null;

                return new ResponseModel<TrainerModel>
                {
                    Data = resData
                };
            }

            return new ResponseModel<TrainerModel>
            {
                Errors = "Fail"
            };
        }

        public async Task<ResponseModel<dynamic>> UpdateInfo(object model)
        {
            var currentUserId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentUserId == null)
                return new ResponseModel<dynamic>
                {
                    Errors = "Not login yet"
                };
            _ = long.TryParse(currentUserId, out long id);
            var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);
            var userModel = JsonConvert.DeserializeObject<User>(model.ToString());
            if (userModel == null)
            {
                return new ResponseModel<dynamic>
                {
                    Errors = "Model invalid"
                };
            }
            // handle update
            SetUpdate(user, userModel);
            _unitOfWork.UserRepository.Update(user);
            if (await _unitOfWork.SaveChangeAsync() <= 0)
            {
                return new ResponseModel<dynamic>
                {
                    Errors = "Update fail"
                };
            }
            switch (role)
            {
                case RoleName.Manager:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<ManagerModel>(user)
                    };
                case RoleName.Staff:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<StaffModel>(user)
                    };
                case RoleName.Trainer:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<TrainerModel>(user)
                    };
                case RoleName.Customer:
                    return new ResponseModel<dynamic>
                    {
                        Data = _mapper.Map<CustomerModel>(user)
                    };
                default:
                    break;
            }

            return new ResponseModel<dynamic>
            {
                Errors = "Not login yet"
            };
        }
        private void SetUpdate(User user, User updatedUser)
        {
            if (updatedUser == null) return;
            user.Password = updatedUser.Password ?? user.Password;
            user.FirstName = updatedUser.FirstName ?? user.FirstName;
            user.LastName = updatedUser.LastName ?? user.LastName;
            user.Email = updatedUser.FirstName ?? user.FirstName;
            user.DOB = updatedUser?.DOB ?? user.DOB;
            user.Address = updatedUser?.Address ?? user.Address;
            user.Certifications = updatedUser?.Certifications ?? user.Certifications;
            user.Position = updatedUser?.Position ?? user.Position;
            user.AvatarUrl = updatedUser?.AvatarUrl ?? user.AvatarUrl;
            user.Height = updatedUser?.Height ?? user.Height;
            user.Weight = updatedUser?.Weight ?? user.Weight;
            user.MuscleRatio = updatedUser?.MuscleRatio ?? user.MuscleRatio;
            user.FatRatio = updatedUser?.FatRatio ?? user.FatRatio;
            user.VisceralFatLevels = updatedUser?.VisceralFatLevels ?? user.VisceralFatLevels;
            user.YOE = updatedUser?.YOE ?? user.YOE;
        }
    }
}
