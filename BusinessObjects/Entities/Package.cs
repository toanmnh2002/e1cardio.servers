﻿namespace BusinessObjects
{
    #region Package
    public class Package : BaseEntity
    {
        public string? ImageUrl { get; set; }
        public string PackageName { get; set; }
        public string? Descriptions { get; set; }
        public double NumberOfDays { get; set; }
        public double NumberOfSessions { get; set; }
        public decimal PackagePrice { get; set; }
        public string Type { get; set; }
        public ICollection<Session>? Sessions { get; set; }
        public PackageStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Schedules { get; set; }
        public long BranchId { get; set; }
        public Branch Branch { get; set; }
        public ICollection<PackageCreator>? PackageCreators { get; set; }
        public ICollection<PackageTrainer>? PackageTrainers { get; set; }
        public ICollection<PackageFollower>? PackageFollowers { get; set; }
    }
    public class Session : BaseEntity
    {
        public string Title { get; set; }
        public string? Outcome { get; set; }
        public string? Descriptions { get; set; }
        public double EnergyPoint { get; set; }
        public ICollection<SessionItem>? SessionItems { get; set; }
        public long PackageId { get; set; }
        public Package Package { get; set; }
        public long BranchId { get; set; }
        public Branch Branch { get; set; }
        public ICollection<SessionTrainer>? SessionTrainers { get; set; }
        public bool IsFinished { get; set; }
    }
    public class SessionItem : BaseEntity
    {
        public string Title { get; set; }
        public string? Description { get; set; }
        public string? ImageUrl { get; set; }
        public long SessionId { get; set; }
        public Session Session { get; set; }
    }
    public class Schedule
    {
        public DayOfWeek Day { get; set; }
        public Time Time { get; set; }
    }
    public class Time
    {
        public int Hour { get; set; }
        public int Minute { get; set; }
    }
    #endregion

    #region Relationship
    public class PackageCreator
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long PackageId { get; set; }
        public Package? Package { get; set; }
    }
    public class PackageTrainer
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long PackageId { get; set; }
        public Package? Package { get; set; }
    }
    public class PackageFollower
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long PackageId { get; set; }
        public Package? Package { get; set; }
    }
    public class SessionTrainer
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long SessionId { get; set; }
        public Session? Session { get; set; }
    }
    #endregion
}
