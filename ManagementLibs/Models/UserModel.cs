﻿public class UserModel
{
    public long Id { get; set; }
    public string Phone { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public double? Height { get; set; }
    public double? Weight { get; set; }
    public double? MuscleRatio { get; set; }
    public double? FatRatio { get; set; }
    public double? VisceralFatLevels { get; set; }
    public string? Certifications { get; set; }
    public double? YOE { get; set; }
    public string? Position { get; set; }
}

#region customer
public class CustomerModel
{
    public long Id { get; set; }
    public string Phone { get; set; }
    public string? Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public double? Height { get; set; }
    public double? Weight { get; set; }
    public double? MuscleRatio { get; set; }
    public double? FatRatio { get; set; }
    public double? VisceralFatLevels { get; set; }
}
public class CreateCustomerModel
{
    public string Phone { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public double? Height { get; set; }
    public double? Weight { get; set; }
    public double? MuscleRatio { get; set; }
    public double? FatRatio { get; set; }
    public double? VisceralFatLevels { get; set; }
}
public class UpdateCustomerModel
{
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public double? Height { get; set; }
    public double? Weight { get; set; }
    public double? MuscleRatio { get; set; }
    public double? FatRatio { get; set; }
    public double? VisceralFatLevels { get; set; }
}
#endregion

#region manager
public class ManagerModel
{
    public long Id { get; set; }
    public string Phone { get; set; }
    public string? Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Position { get; set; }
}
public class CreateManagerModel
{
    public string Phone { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Position { get; set; }
}
public class UpdateManagerModel
{
    public string? Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Position { get; set; }
}
#endregion

#region staff
public class StaffModel
{
    public long Id { get; set; }    
    public string Phone { get; set; }
    public string? Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Position { get; set; }
}
public class CreateStaffModel
{
    public string Phone { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Position { get; set; }
}
public class UpdateStaffModel
{
    public string? Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Position { get; set; }
}
#endregion

#region trainer
public class TrainerModel
{
    public long Id { get; set; }
    public string Phone { get; set; }
    public string? Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Certifications { get; set; }
    public double? YOE { get; set; }
}
public class CreateTrainerModel
{
    public string Phone { get; set; }
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Certifications { get; set; }
    public double? YOE { get; set; }
}
public class UpdateTrainerModel
{
    public string Password { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public DateTime DOB { get; set; }
    public string? Address { get; set; }
    public string? AvatarUrl { get; set; }
    public string? Certifications { get; set; }
    public double? YOE { get; set; }
}
#endregion
