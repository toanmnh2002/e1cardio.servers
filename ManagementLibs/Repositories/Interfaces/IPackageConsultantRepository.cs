﻿using BusinessObjects;

namespace ManagementLibs.Repositories
{
    public interface IPackageConsultantRepository
    {
        Task CreateAsync(PackageConsultant obj);
        Task<List<PackageConsultant>> GetPackageConsultants(string? query, 
            long userId, 
            int pageIndex = 1, 
            int pageSize = 10, 
            bool isManager = false);
        Task<PackageConsultant> GetAsync(long id);
        void Update(PackageConsultant obj);
        void Delete(PackageConsultant obj);
    }
}
