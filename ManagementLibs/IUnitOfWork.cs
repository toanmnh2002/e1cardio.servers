﻿using ManagementLibs.Repositories;

namespace ManagementLibs
{
    public interface IUnitOfWork
    {
        public IUserRepository UserRepository { get; }
        public IBranchRepository BranchRepository { get; }
        public IDemoPackageRepository DemoPackageRepository { get; }
        public IPackageConsultantRepository PackageConsultantRepository { get; }
        public ITaskRepository TaskRepository { get; }
        Task<int> SaveChangeAsync();
    }
}
