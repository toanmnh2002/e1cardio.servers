﻿namespace BusinessObjects
{
    public class ScheduleChangeRequest : BaseEntity
    {
        public string Reason { get; set; }
        public long SessionId { get; set; }
        public Session? Session { get; set; }
        public long UserId { get; set; }
        public User? User { get; set; }
        public string RequestDetails { get; set; }
    }
}
