﻿using BusinessObjects;
using Microsoft.EntityFrameworkCore;

namespace ManagementLibs.Repositories
{
    public class BranchRepository : IBranchRepository
    {
        private readonly AppDbContext _context;
        public BranchRepository(AppDbContext context)
        {
            _context = context;
        }
        public async Task CreateAsync(Branch branch)
        {
            branch.CreatedDate = DateTime.UtcNow;
            branch.UpdatedDate = DateTime.UtcNow;
            await _context.Branches.AddAsync(branch);
        }

        public void Delete(Branch branch)
        {
            _context.Branches.Remove(branch);
        }

        public async Task<Branch> GetAsync(long id)
        {
            return await _context.Branches
                .Include(x => x.DemoPackages)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<List<Branch>> GetBranches(string? query, int pageIndex = 1, int pageSize = 10)
        {
            if (!string.IsNullOrEmpty(query))
                return await _context.Branches
                    .Where(x =>
                        x.BranchName.Contains(query)
                        || x.Location.Contains(query)
                        || x.Phone.Contains(query))
                    .OrderByDescending(x => x.UpdatedDate)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync();

            return await _context.Branches
                    .OrderByDescending(x => x.UpdatedDate)
                    .Skip((pageIndex - 1) * pageSize)
                    .Take(pageSize)
                    .AsNoTracking()
                    .ToListAsync();
        }

        public void Update(Branch branch)
        {
            branch.UpdatedDate = DateTime.UtcNow;
            _context.Branches.Update(branch);
        }
    }
}
