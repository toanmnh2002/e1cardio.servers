﻿using AutoMapper;
using BusinessObjects;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace ManagementLibs.Services
{
    public class PackageConsultantService : IPackageConsultantService
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public PackageConsultantService(IUnitOfWork unitOfWork,
            IHttpContextAccessor httpContext,
            IMapper mapper)
        {
            _httpContext = httpContext;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<ResponseModel<PackageConsultantModel>> CreatePackageConsultant(CreatePackageConsultantModel model)
        {
            if (model.Consultee == null)
            {
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Consultee not found"
                };
            }
            var currentUserId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentUserId == null)
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Not login yet"
                };
            _ = long.TryParse(currentUserId, out long id);
            var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);
            if (user == null)
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Not found staff"
                };

            var package = await _unitOfWork.DemoPackageRepository.GetDemoPackageAsync(model.DemoPackageId);
            if (package == null)
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Not found package"
                };

            var consultant = new PackageConsultant();
            consultant.DemoPackage = package;
            consultant.StartDate = model.StartDate;
            consultant.EndDate = model.EndDate;
            consultant.Schedules = JsonConvert.SerializeObject(model.Schedules);
            consultant.Consultants ??= [];
            consultant.Consultants.Add(new Consultants
            {
                User = user
            });
            // get customer info
            var customer = await _unitOfWork.UserRepository.GetByPhone(model.Consultee.Phone);
            if (customer != null)
            {
                consultant.Consultees ??= [];
                consultant.Consultees.Add(new Consultees
                {
                    User = customer
                });
            }
            else
            {
                var newCustomer = new User();
                newCustomer.Phone = model.Consultee.Phone;
                newCustomer.Password = "1";
                newCustomer.FirstName = model.Consultee.FirstName;
                newCustomer.LastName = model.Consultee.LastName;
                newCustomer.Email = model.Consultee.Email;
                newCustomer.Roles ??= [];
                newCustomer.Roles.Add(new Role
                {
                    RoleName = RoleName.Customer,
                    CreatedDate = DateTime.UtcNow,
                    UpdatedDate = DateTime.UtcNow,
                });

                await _unitOfWork.UserRepository.CreateAsync(newCustomer);
                consultant.Consultees ??= [];
                consultant.Consultees.Add(new Consultees
                {
                    User = newCustomer
                });
            }

            await _unitOfWork.PackageConsultantRepository.CreateAsync(consultant);
            if (await _unitOfWork.SaveChangeAsync() < 0)
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Create fail"
                };

            var responseData = GetPackageConsultantModel(consultant);
            return new ResponseModel<PackageConsultantModel>
            {
                Data = responseData
            };
        }

        public async Task<ResponseModel<bool>> DeleteConsultant(long id)
        {
            var consultant = await _unitOfWork.PackageConsultantRepository.GetAsync(id);
            if (consultant == null)
                return new ResponseModel<bool>
                {
                    Data = true
                };
            //_unitOfWork.PackageConsultantRepository.Delete(consultant);
            if (await _unitOfWork.SaveChangeAsync() > 0)
            {
                return new ResponseModel<bool>
                {
                    Data = true
                };
            }

            return new ResponseModel<bool>
            {
                Errors = "Error"
            };
        }

        public async Task<ResponseModel<PackageConsultantModel>> GetPackageConsultant(long id)
        {
            var consultant = await _unitOfWork.PackageConsultantRepository.GetAsync(id);
            if (consultant == null)
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Not found"
                };

            return new ResponseModel<PackageConsultantModel>
            {
                Data = GetPackageConsultantModel(consultant)
            };
        }

        public async Task<ResponseModel<List<PackageConsultantModel>>> GetPackageConsultants(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var currentUserId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentUserId == null)
                return new ResponseModel<List<PackageConsultantModel>>
                {
                    Errors = "Not login yet"
                };
            _ = long.TryParse(currentUserId, out long id);
            var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);
            if (user == null)
                return new ResponseModel<List<PackageConsultantModel>>
                {
                    Errors = "Not found staff"
                };
            var consultants = await _unitOfWork.PackageConsultantRepository.GetPackageConsultants(query,
                user.Id,
                pageIndex,
                pageSize,
                role is RoleName.Manager);

            var responseData = new List<PackageConsultantModel>();
            consultants.ForEach(consultant =>
            {
                var model = GetPackageConsultantModel(consultant);
                responseData.Add(model);
            });

            return new ResponseModel<List<PackageConsultantModel>>
            {
                Data = responseData
            };
        }

        public async Task<ResponseModel<PackageConsultantModel>> UpdateConsultant(long id, UpdatePackageConsultantModel model)
        {
            var consultant = await _unitOfWork.PackageConsultantRepository.GetAsync(id);
            if (consultant == null)
                return new ResponseModel<PackageConsultantModel>
                {
                    Errors = "Not found"
                };

            consultant.DemoPackageId = model.DemoPackageId;
            consultant.StartDate = model.StartDate;
            consultant.EndDate = model.EndDate;
            consultant.Schedules = JsonConvert.SerializeObject(model.Schedules);

            _unitOfWork.PackageConsultantRepository.Update(consultant);
            if (await _unitOfWork.SaveChangeAsync() > 0)
            {
                return new ResponseModel<PackageConsultantModel>
                {
                    Data = GetPackageConsultantModel(consultant)
                };
            }

            return new ResponseModel<PackageConsultantModel>
            {
                Errors = "Error"
            };
        }

        private PackageConsultantModel GetPackageConsultantModel(PackageConsultant obj)
        {
            var model = new PackageConsultantModel();
            model.Id = obj.Id;
            model.DemoPackage = _mapper.Map<DemoPackageModel>(obj.DemoPackage);
            model.StartDate = obj.StartDate;
            model.EndDate = obj.EndDate;
            model.Schedules = JsonConvert.DeserializeObject<List<Schedule>>(obj.Schedules);
            model.IsReceived = obj.IsReceived;
            model.Consultants = _mapper.Map<ConsultantModel>(obj.Consultants.First().User);
            model.Consultees = _mapper.Map<ConsultantModel>(obj.Consultees.First().User);

            return model;
        }
    }
}
