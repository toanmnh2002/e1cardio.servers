﻿using BusinessObjects;
using System.Text.Json.Serialization;
using TaskStatus = BusinessObjects.TaskStatus;


public class TaskModel
{
    public long Id { get; set; }
    public string TaskName { get; set; }
    public string TaskInfomations { get; set; }
    public TaskAssignerModel? Assigners { get; set; }
    public TaskReceiverModel? Receivers { get; set; }
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public TaskPriority Priority { get; set; }
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public TaskStatus Status { get; set; }
    public DateTime CreatedDate { get; set; }
    public DateTime UpdatedDate { get; set; }
}

public class CreateTaskModel
{
    public string TaskName { get; set; }
    public string TaskInfomations { get; set; }
    public long ReceiverId { get; set; }
    public TaskPriority Priority { get; set; }
}

public class UpdateTaskModel
{
    public string TaskName { get; set; }
    public string? TaskInfomations { get; set; }
    public TaskStatus Status { get; set; }
    public TaskPriority Priority { get; set; }
    public long ReceiverId { get; set; }
}

public class TaskAssignerModel
{
    public string Phone { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public string Address { get; set; }
}
public class TaskReceiverModel
{
    public string Phone { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public string Address { get; set; }
}


