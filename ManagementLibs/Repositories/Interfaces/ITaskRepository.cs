﻿using BusinessObjects;

namespace ManagementLibs.Repositories
{
    public interface ITaskRepository
    {
        Task CreateAsync(Tasks task);
        void Delete(Tasks task);
        void Update(Tasks task);
        Task<Tasks> GetAsync(long id);
        Task<List<Tasks>> GetTasks(string? query, long assignerId, int pageIndex = 1, int pageSize = 10, bool isManager = false);
    }
}
