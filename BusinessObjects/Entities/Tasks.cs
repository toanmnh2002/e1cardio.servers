﻿namespace BusinessObjects
{
    public class Tasks : BaseEntity
    {
        public string TaskName { get; set; }
        public TaskPriority Priority { get; set; }
        public string TaskInfomations { get; set; } // package consultant info
        public ICollection<TaskAssigner>? Assigners { get; set; }
        public ICollection<TaskReceiver>? Receivers { get; set; }
        public TaskStatus Status { get; set; }
    }
    public class TaskAssigner
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long TaskId { get; set; }
        public Tasks? Task { get; set; }
    }
    public class TaskReceiver
    {
        public long UserId { get; set; }
        public User? User { get; set; }
        public long TaskId { get; set; }
        public Tasks? Task { get; set; }
    }
}
