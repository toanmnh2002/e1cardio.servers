﻿using ManagementLibs.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ManagementApis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BranchController : ControllerBase
    {
        private readonly IBranchService _service;
        public BranchController(IBranchService service)
        {
            _service = service;
        }

        [Authorize(Policy = "Staff")]
        [HttpGet]
        public async Task<IActionResult> GetBranches(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetBranches(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetBranch(long id)
        {
            var result = await _service.GetBranchById(id);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPost]
        public async Task<IActionResult> CreateBranch([FromBody] CreateBranchModel model)
        {
            var result = await _service.CreateBranch(model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateBranch(long id, [FromBody] CreateBranchModel model)
        {
            var result = await _service.UpdateBranch(id, model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteBranch(long id)
        {
            var result = await _service.DeleteBranch(id);
            return Ok(result);
        }
    }
}
