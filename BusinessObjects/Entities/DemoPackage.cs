﻿namespace BusinessObjects
{
    public class DemoPackage : BaseEntity
    {
        public string? ImageUrl { get; set; }
        public string PackageName { get; set; }
        public string Descriptions { get; set; }
        public double NumberOfDays { get; set; }
        public double NumberOfSessions { get; set; }
        public decimal PackagePrice { get; set; }
        public string Type { get; set; }
        public long BranchId { get; set; }
        public Branch Branch { get; set; }
        public ICollection<PackageConsultant>? PackageConsultants { get; set; }
    }
}
