﻿using AutoMapper;
using BusinessObjects;
using Microsoft.AspNetCore.Http;
using TaskStatus = BusinessObjects.TaskStatus;

namespace ManagementLibs.Services
{
    public class TaskService : ITaskService
    {
        private readonly IHttpContextAccessor _httpContext;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TaskService(IHttpContextAccessor httpContext, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _httpContext = httpContext;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<ResponseModel<List<TaskModel>>> GetTasks(string? query, int pageIndex = 1, int pageSize = 10)
        {
            {
                var currentUserId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
                if (currentUserId == null)
                    return new ResponseModel<List<TaskModel>>
                    {
                        Errors = "Not login yet"
                    };

                _ = long.TryParse(currentUserId, out long id);
                var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);

                if (user == null)
                    return new ResponseModel<List<TaskModel>>
                    {
                        Errors = "Not found staff"
                    };

                var tasks = await _unitOfWork.TaskRepository.GetTasks(query,
                user.Id,
                pageIndex,
                pageSize,
                role is RoleName.Manager);

                var responseData = new List<TaskModel>();
                tasks.ForEach(task =>
                {
                    var model = GetTaskModel(task);
                    responseData.Add(model);
                });

                return new ResponseModel<List<TaskModel>>
                {
                    Data = responseData
                };
            }
        }
        private TaskModel GetTaskModel(Tasks obj)
        {
            var model = new TaskModel();
            model.Id = obj.Id;
            model.TaskName = obj.TaskName;
            model.TaskInfomations = obj.TaskInfomations;
            model.Priority = obj.Priority;
            model.CreatedDate = obj.CreatedDate;
            model.UpdatedDate = obj.UpdatedDate;

            if (obj.Assigners.Any())
            {
                model.Assigners = _mapper.Map<TaskAssignerModel>(obj.Assigners.FirstOrDefault().User);
            }

            if (obj.Receivers.Any())
            {
                model.Receivers = _mapper.Map<TaskReceiverModel>(obj.Receivers.FirstOrDefault().User);
            }
            model.Status = obj.Status;

            return model;
        }

        public async Task<ResponseModel<TaskModel>> GetTask(long id)
        {
            var task = await _unitOfWork.TaskRepository.GetAsync(id);
            if (task is null)
                return new ResponseModel<TaskModel>
                {
                    Errors = "Not found"
                };

            return new ResponseModel<TaskModel>
            {
                Data = GetTaskModel(task)
            };
        }

        public async Task<ResponseModel<TaskModel>> UpdateTask(long id, UpdateTaskModel model)
        {
            var task = await _unitOfWork.TaskRepository.GetAsync(id);
            if (task is null)
                return new ResponseModel<TaskModel>
                {
                    Errors = "Not found"
                };

            task.TaskInfomations = model.TaskInfomations;
            task.Status = model.Status;
            task.TaskName = model.TaskName;
            task.Priority = model.Priority;

            var trainer = await _unitOfWork.UserRepository.GetByIdAsync(model.ReceiverId, RoleName.Trainer);

            if (trainer is null)
            {
                return new ResponseModel<TaskModel>
                {
                    Errors = "Not found trainer"
                };
            }

            task.Receivers ??= [];
            task.Receivers = new List<TaskReceiver> { new TaskReceiver { UserId = trainer.Id } };

            _unitOfWork.TaskRepository.Update(task);
            if (await _unitOfWork.SaveChangeAsync() > 0)
            {
                return new ResponseModel<TaskModel>
                {
                    Data = GetTaskModel(task)
                };
            }

            return new ResponseModel<TaskModel>
            {
                Errors = "Error"
            };
        }

        public async Task<ResponseModel<bool>> DeleteTask(long id)
        {
            var task = await _unitOfWork.TaskRepository.GetAsync(id);
            if (task is null)
                return new ResponseModel<bool>
                {
                    Data = true
                };
            _unitOfWork.TaskRepository.Delete(task);
            if (await _unitOfWork.SaveChangeAsync() > 0)
            {
                return new ResponseModel<bool>
                {
                    Data = true
                };
            }

            return new ResponseModel<bool>
            {
                Errors = "Error"
            };
        }

        public async Task<ResponseModel<TaskModel>> CreateTask(CreateTaskModel model)
        {
            var currentUserId = _httpContext.HttpContext?.User?.Claims.FirstOrDefault(x => x.Type == "id")?.Value;
            if (currentUserId == null)
                return new ResponseModel<TaskModel>
                {
                    Errors = "Not login yet"
                };

            _ = long.TryParse(currentUserId, out long id);
            var (role, user) = await _unitOfWork.UserRepository.GetDynamicUser(id);
            if (user == null)
                return new ResponseModel<TaskModel>
                {
                    Errors = "Not found staff"
                };

            var task = new Tasks();
            task.TaskInfomations = model.TaskInfomations;
            task.TaskName = model.TaskName;
            task.Priority = model.Priority;
            task.Assigners ??= [];
            task.Assigners.Add(new TaskAssigner
            {
                UserId = id,
                User = user,
                Task = task
            });

            var trainer = await _unitOfWork.UserRepository.GetByIdAsync(model.ReceiverId, RoleName.Trainer);

            if (trainer == null)
                return new ResponseModel<TaskModel>
                {
                    Errors = "Not found trainer!"
                };

            if (trainer != null)
            {
                task.Receivers ??= [];
                task.Receivers.Add(new TaskReceiver
                {
                    User = trainer,
                    Task = task,
                    UserId = trainer.Id
                });
            }

            task.Status = TaskStatus.InProcess;

            await _unitOfWork.TaskRepository.CreateAsync(task);
            if (await _unitOfWork.SaveChangeAsync() > 0)
            {
                return new ResponseModel<TaskModel>
                {
                    Data = GetTaskModel(task)
                };
            }

            return new ResponseModel<TaskModel>
            {
                Errors = "Error"
            };
        }
    }
}
