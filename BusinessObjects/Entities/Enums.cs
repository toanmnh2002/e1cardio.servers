﻿namespace BusinessObjects
{
    public enum PackageStatus
    {
        InProcess = 1,
        Finished = 2
    }
    public enum RoleName
    {
        Manager = 1,
        Staff = 2,
        Trainer = 3,
        Customer = 4,
    }
    public enum TaskStatus
    {
        InProcess = 1,
        Finished = 2
    }
    public enum TaskPriority
    {
        Low = 1,
        Medium = 2,
        High = 3
    }
}
