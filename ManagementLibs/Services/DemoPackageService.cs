﻿using AutoMapper;
using BusinessObjects;

namespace ManagementLibs.Services
{
    public class DemoPackageService : IDemoPackageService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public DemoPackageService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<ResponseModel<DemoPackageModel>> CreateDemoPackage(CreateDemoPackageModel model)
        {
            //validate model
            var demoPackage = _mapper.Map<DemoPackage>(model);
            var branch = await _unitOfWork.BranchRepository.GetAsync(model.BranchId);
            if (branch == null)
                return new ResponseModel<DemoPackageModel>
                {
                    Errors = "Not found branch"
                };

            demoPackage.Branch = branch;
            await _unitOfWork.DemoPackageRepository.CreateAsync(demoPackage);
            if (await _unitOfWork.SaveChangeAsync() > 0)
                return new ResponseModel<DemoPackageModel>
                {
                    Data = _mapper.Map<DemoPackageModel>(demoPackage)
                };

            return new ResponseModel<DemoPackageModel>
            {
                Errors = "Create fail"
            };
        }

        public async Task<ResponseModel<bool>> DeleteDemoPackage(long id)
        {
            var demoPackage = await _unitOfWork.DemoPackageRepository.GetDemoPackageAsync(id);
            if (demoPackage == null)
                return new ResponseModel<bool>
                {
                    Data = true
                };
            _unitOfWork.DemoPackageRepository.Delete(demoPackage);
            if (await _unitOfWork.SaveChangeAsync() > 0)
                return new ResponseModel<bool>
                {
                    Data = true
                };

            return new ResponseModel<bool>
            {
                Errors = "Delete fail"
            };
        }

        public async Task<ResponseModel<DemoPackageModel>> GetPackage(long id)
        {
            var demoPackage = await _unitOfWork.DemoPackageRepository.GetDemoPackageAsync(id);
            if (demoPackage == null)
                return new ResponseModel<DemoPackageModel>
                {
                    Errors = "Not found"
                };

            return new ResponseModel<DemoPackageModel>
            {
                Data = _mapper.Map<DemoPackageModel>(demoPackage)
            };
        }

        public async Task<ResponseModel<List<DemoPackageModel>>> GetPackages(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var branches = await _unitOfWork.DemoPackageRepository.GetDemoPackagesAsync(query, pageIndex, pageSize);

            return new ResponseModel<List<DemoPackageModel>>
            {
                Data = _mapper.Map<List<DemoPackageModel>>(branches)
            };
        }

        public async Task<ResponseModel<DemoPackageModel>> UpdateDemoPackage(long id, UpdateDemoPackageModel model)
        {
            // validate model
            var package = await _unitOfWork.DemoPackageRepository.GetDemoPackageAsync(id);
            if (package == null)
                return new ResponseModel<DemoPackageModel>
                {
                    Errors = "Not found"
                };
            var branch = await _unitOfWork.BranchRepository.GetAsync(model.BranchId);
            if (branch == null)
                return new ResponseModel<DemoPackageModel>
                {
                    Errors = "Not found branch"
                };

            package.ImageUrl = model.ImageUrl;
            package.PackageName = model.PackageName;
            package.Descriptions = model.Descriptions;
            package.NumberOfDays = model.NumberOfDays;
            package.NumberOfSessions = model.NumberOfSessions;
            package.PackagePrice = model.PackagePrice;
            package.Type = model.Type;
            package.Branch = branch;

            _unitOfWork.DemoPackageRepository.Update(package);
            if (await _unitOfWork.SaveChangeAsync() > 0)
                return new ResponseModel<DemoPackageModel>
                {
                    Data = _mapper.Map<DemoPackageModel>(package)
                };

            return new ResponseModel<DemoPackageModel>
            {
                Errors = "Update fail"
            };
        }
    }
}
