﻿using ManagementLibs.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ManagementApis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _service;
        public UserController(IUserService service)
        {
            _service = service;
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUser(long id)
        {
            var result = await _service.GetUser(id);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("customers")]
        public async Task<IActionResult> GetCustomers(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetCustomers(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpGet("staffs")]
        public async Task<IActionResult> GetStaffs(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetStaffs(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpGet("trainers")]
        public async Task<IActionResult> GetTrainers(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetTrainers(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpGet("managers")]
        public async Task<IActionResult> GetManagers(string? query, int pageIndex = 1, int pageSize = 10)
        {
            var result = await _service.GetManagers(query, pageIndex, pageSize);
            return Ok(result);
        }

        [Authorize(Policy = "Staff")]
        [HttpPost("new-customer")]
        public async Task<IActionResult> NewCustomer([FromBody] CreateCustomerModel model)
        {
            var result = await _service.NewCustomer(model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPost("new-manager")]
        public async Task<IActionResult> NewManager([FromBody] CreateManagerModel model)
        {
            var result = await _service.NewManager(model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPost("new-staff")]
        public async Task<IActionResult> NewStaff([FromBody] CreateStaffModel model)
        {
            var result = await _service.NewStaff(model);
            return Ok(result);
        }

        [Authorize(Policy = "Manager")]
        [HttpPost("new-trainer")]
        public async Task<IActionResult> NewTrainer([FromBody] CreateTrainerModel model)
        {
            var result = await _service.NewTrainer(model);
            return Ok(result);
        }
    }
}
