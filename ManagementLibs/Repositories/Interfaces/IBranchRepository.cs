﻿using BusinessObjects;

namespace ManagementLibs.Repositories
{
    public interface IBranchRepository
    {
        Task CreateAsync(Branch branch);
        void Update(Branch branch);
        void Delete(Branch branch);
        Task<Branch> GetAsync(long id);
        Task<List<Branch>> GetBranches(string? query, int pageIndex = 1, int pageSize = 10);
    }
}
