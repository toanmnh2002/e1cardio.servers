﻿namespace BusinessObjects
{
    public class Branch : BaseEntity
    {
        public string BranchName { get; set; }
        public string Location { get; set; }
        public string Phone { get; set; }
        public ICollection<DemoPackage>? DemoPackages { get; set; }
        public ICollection<Package>? Packages { get; set; }
    }
}
