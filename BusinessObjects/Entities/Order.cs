﻿namespace BusinessObjects
{
    public class Order : BaseEntity
    {
        public DateTime OrderDate { get; set; }
        public decimal OrderAmount { get; set; }
        public long UserId { get; set; }
        public User User { get; set; }
        public long PackageId { get; set; }
        public Package Package { get; set; }
    }
}
