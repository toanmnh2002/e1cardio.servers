﻿using ManagementLibs.Services;
using Microsoft.AspNetCore.Mvc;

namespace ManagementApis.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _service;
        public AuthController(IUserService service)
        {
            _service = service;
        }

        [HttpPost("/login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var result = await _service.Login(model);
            return Ok(result);
        }

        [HttpGet("info")]
        public async Task<IActionResult> GetInfo()
        {
            var result = await _service.GetCurrentUser();
            return Ok(result);
        }

        [HttpPut("update")]
        public async Task<IActionResult> UpdateInfo(object model)
        {
            var result = await _service.UpdateInfo(model);
            return Ok(result);
        }
    }
}
