﻿using BusinessObjects;
using ManagementLibs.Repositories;

namespace ManagementLibs
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;
        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }
        public IUserRepository UserRepository => new UserRepository(_context);

        public IBranchRepository BranchRepository => new BranchRepository(_context);

        public IDemoPackageRepository DemoPackageRepository => new DemoPackageRepository(_context);

        public IPackageConsultantRepository PackageConsultantRepository => new PackageConsultantRepository(_context);

        public ITaskRepository TaskRepository => new TaskRepository(_context);

        public async Task<int> SaveChangeAsync()
        {
            return await _context.SaveChangesAsync();
        }
    }
}
