﻿namespace ManagementLibs.Services
{
    public interface IUserService
    {
        Task<ResponseModel<string>> Login(LoginModel model);
        Task<ResponseModel<dynamic>> GetCurrentUser();
        Task<ResponseModel<dynamic>> UpdateInfo(object model);
        Task<ResponseModel<dynamic>> GetUser(long id);
        Task<ResponseModel<List<CustomerModel>>> GetCustomers(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<List<StaffModel>>> GetStaffs(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<List<TrainerModel>>> GetTrainers(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<List<ManagerModel>>> GetManagers(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<CustomerModel>> NewCustomer(CreateCustomerModel model);
        Task<ResponseModel<ManagerModel>> NewManager(CreateManagerModel model);
        Task<ResponseModel<StaffModel>> NewStaff(CreateStaffModel model);
        Task<ResponseModel<TrainerModel>> NewTrainer(CreateTrainerModel model);
    }
}
