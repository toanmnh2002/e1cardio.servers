﻿namespace BusinessObjects
{
    public class User : BaseEntity
    {
        public string Phone { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string? Email { get; set; }
        public DateTime DOB { get; set; }
        public string? Address { get; set; }
        public string? AvatarUrl { get; set; }
        //customer
        public double? Height { get; set; }
        public double? Weight { get; set; }
        public double? MuscleRatio { get; set; }
        public double? FatRatio { get; set; }
        public double? VisceralFatLevels { get; set; }
        // trainer
        public string? Certifications { get; set; }
        public double? YOE { get; set; }
        // staff & manager
        public string? Position { get; set; }
        // role
        public ICollection<Role> Roles { get; set; }
        // relationship
        public ICollection<Consultants>? Consultants { get; set; }
        public ICollection<Consultees>? Consultees { get; set; }
        public ICollection<PackageCreator>? PackageCreators { get; set; }
        public ICollection<PackageTrainer>? PackageTrainers { get; set; }
        public ICollection<PackageFollower>? PackageFollowers { get; set; }
        public ICollection<SessionTrainer>? SessionTrainers { get; set; }
        public ICollection<ScheduleChangeRequest>? ScheduleChanges { get; set; }
        public ICollection<TaskAssigner>? TaskAssigners { get; set; }
        public ICollection<TaskReceiver>? TaskReceivers { get; set; }
        public ICollection<TrainingTracking>? TrainingTrackings { get; set; }
        public ICollection<Order>? Orders { get; set; }
    }
}
