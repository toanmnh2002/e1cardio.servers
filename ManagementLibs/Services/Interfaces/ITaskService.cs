﻿namespace ManagementLibs.Services
{
    public interface ITaskService
    {
        Task<ResponseModel<TaskModel>> CreateTask(CreateTaskModel model);
        Task<ResponseModel<bool>> DeleteTask(long id);
        Task<ResponseModel<TaskModel>> GetTask(long id);
        Task<ResponseModel<List<TaskModel>>> GetTasks(string? query, int pageIndex = 1, int pageSize = 10);
        Task<ResponseModel<TaskModel>> UpdateTask(long id, UpdateTaskModel model);
    }
}